/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay;

import com.tfg.relay.topology.Relay;
import com.tfg.relay.utils.MulticastInputStream;
import com.tfg.relay.utils.SocketReader;
import com.tfg.relay.utils.SocketWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author eryalus
 */
public class Constants {

    public static String SERVER_MULTICAST_IP = "224.0.0.1";
    public static int SERVER_MULTICAST_PORT = 4446;
    public static int RELAY_ID;
    public static final String TOPOLOGY_JSON_PATH = "res/topology.json";
    public static final Logger LOG = LoggerFactory.getLogger("TFG relay");
    public static Relay RELAY;
    public static SocketWriter WRITER;
    public static SocketReader READER;
    public static MulticastInputStream MIS;

    public static final class MESSAGE_ACTIONS_TYPE {

        public static final int SETUP = 1, PLACEMENT_PART = 2, PLACEMENT_ENDED = 3, PHASE_1_MESSAGE = 4, PHASE_2_MESSAGE = 5, DELIVEY_ENDED = 6;

    }
}
