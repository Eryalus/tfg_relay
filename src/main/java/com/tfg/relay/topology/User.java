/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.topology;

import java.util.ArrayList;

/**
 *
 * @author eryalus
 */
public class User implements Comparable<User> {

    private long cacheSize;
    private ArrayList<Relay> connectedRelays = new ArrayList<>();
    private int id;

    public long getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(long cacheSize) {
        this.cacheSize = cacheSize;
    }

    public ArrayList<Relay> getConnectedRelays() {
        return connectedRelays;
    }

    public void setConnectedRelays(ArrayList<Relay> connectedRelays) {
        this.connectedRelays = connectedRelays;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(User arg0) {
        return id - arg0.id;
    }

}
