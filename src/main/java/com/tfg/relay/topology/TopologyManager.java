/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.topology;

import com.tfg.relay.topology.JSON.TopologyJSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.relay.Constants;
import com.tfg.relay.topology.JSON.RelayJSON;
import com.tfg.relay.topology.JSON.UserJSON;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author eryalus
 */
public class TopologyManager {

    public Topology readTopology() {
        TopologyJSON topologyJSON;
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = new File(Constants.TOPOLOGY_JSON_PATH);
            topologyJSON = mapper.readValue(file, TopologyJSON.class);
        } catch (IOException ex) {
            Constants.LOG.error("Can not read topology", ex);
            topologyJSON = null;
        }
        if (topologyJSON != null) {
            Constants.LOG.info("Processing data...");
            Topology topology = new Topology();
            //setting relays w/o users
            for (RelayJSON relay : topologyJSON.getRelays()) {
                Relay r = new Relay();
                r.setId(relay.getId());
                r.setIp(relay.getIp());
                r.setPort(relay.getPort());
                topology.getRelays().add(r);
            }
            //setting users
            for (UserJSON user : topologyJSON.getUsers()) {
                User u = new User();
                u.setCacheSize(user.getCacheSize());
                u.setId(user.getId());
                for (int rID : user.getConnectedRelays()) {
                    for (Relay r : topology.getRelays()) {
                        if (r.getId() == rID) {
                            u.getConnectedRelays().add(r);
                            //setting user to relay
                            r.getConnectedUsers().put(u.getId(), u);
                            break;
                        }
                    }
                }
                topology.getUsers().put(u.getId(), u);
            }
            return topology;
        } else {
            return null;
        }
    }
}
