/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.topology.JSON;

/**
 *
 * @author eryalus
 */
public class UserJSON {

    private long cacheSize;
    private int[] connectedRelays;
    private int id;

    public long getCacheSize() {
        return cacheSize;
    }

    public void setCacheSize(long cacheSize) {
        this.cacheSize = cacheSize;
    }

    public int[] getConnectedRelays() {
        return connectedRelays;
    }

    public void setConnectedRelays(int[] connectedRelays) {
        this.connectedRelays = connectedRelays;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
