/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.topology.JSON;

/**
 *
 * @author eryalus
 */
public class TopologyJSON {

    private RelayJSON[] relays;
    private UserJSON[] users;

    public RelayJSON[] getRelays() {
        return relays;
    }

    public void setRelays(RelayJSON[] relays) {
        this.relays = relays;
    }

    public UserJSON[] getUsers() {
        return users;
    }

    public void setUsers(UserJSON[] users) {
        this.users = users;
    }

}
