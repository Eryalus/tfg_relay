/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.topology;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class Topology {

    private HashMap<Integer, User> users = new HashMap<>();
    private ArrayList<Relay> relays = new ArrayList<>();

    public HashMap<Integer, User> getUsers() {
        return users;
    }

    public void setUsers(HashMap<Integer, User> users) {
        this.users = users;
    }

    public ArrayList<Relay> getRelays() {
        return relays;
    }

    public void setRelays(ArrayList<Relay> relays) {
        this.relays = relays;
    }

}
