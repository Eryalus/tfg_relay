/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay;

import com.tfg.relay.messages.MessageProcessor;
import com.tfg.relay.topology.Relay;
import com.tfg.relay.topology.Topology;
import com.tfg.relay.topology.TopologyManager;
import com.tfg.relay.topology.User;
import com.tfg.relay.utils.MulticastInputStream;
import com.tfg.relay.utils.SocketReader;
import com.tfg.relay.utils.SocketWriter;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class Main {

    MulticastSocket socket;

    public static void main(String[] args) {
        try {
            new Main().run(args);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run(String[] args) throws IOException {
        if (!setupVars(args)) {
            return;
        }
        if (!parseJSONS()) {
            return;
        }
        Constants.LOG.info("Starting multicast sender on " + Constants.RELAY.getIp() + ":" + Constants.RELAY.getPort() + "...");
        try {
            DatagramSocket soc = new DatagramSocket();
            Constants.WRITER = new SocketWriter(soc, Constants.RELAY.getIp());
        } catch (Exception ex) {
            Constants.LOG.error("Can not create multicast sender", ex);
            return;
        }
        Constants.LOG.info("Multicast sender started.");

        Constants.LOG.info("Creating MessageProcessor...");
        MessageProcessor MessageProcessor = new MessageProcessor();
        Constants.LOG.info("MessageProcessor created.");

        Constants.LOG.info("Starting multicast socket on " + Constants.SERVER_MULTICAST_IP + ":" + Constants.SERVER_MULTICAST_PORT + "...");
        socket = new MulticastSocket(Constants.SERVER_MULTICAST_PORT);
        InetAddress group = InetAddress.getByName(Constants.SERVER_MULTICAST_IP);
        socket.joinGroup(group);
        Constants.MIS = new MulticastInputStream(socket);
        Constants.READER = new SocketReader(Constants.MIS);
        Constants.LOG.info("Multicast socket started.");

        while (true) {
            //all of the messages will start with an int32 as message type
            int type = Constants.READER.readInt32();
            Constants.LOG.info("Received type: " + type);
            MessageProcessor.readMessageForType(type);
        }
    }

    public static boolean parseJSONS() {
        Constants.LOG.info("Processing topology...");
        Topology topology = new TopologyManager().readTopology();
        Constants.LOG.info("Topology readed.");
        if (topology == null) {
            return false;
        } else {
            Constants.RELAY = null;
            for (Relay relay : topology.getRelays()) {
                if (relay.getId() == Constants.RELAY_ID) {
                    Constants.RELAY = relay;
                    Constants.LOG.info("Relay found on topology.");
                    String logTxt = "";
                    for (User u : relay.getConnectedUsers().values()) {
                        logTxt += "{\"id\":" + u.getId() + ",\"cacheSize:\":" + u.getCacheSize() + "}";
                    }
                    Constants.LOG.info("USERS: " + logTxt);
                    break;
                }
            }
        }
        return Constants.RELAY != null;
    }

    public static boolean setupVars(String[] args) {
        if (args.length != 1) {
            System.err.println("java -jar Relay.jar [relay_id]");
            return false;
        } else {
            try {
                int id = Integer.parseInt(args[0]);
                Constants.RELAY_ID = id;
                Constants.LOG.info("Relay id: " + Constants.RELAY_ID);
                return true;
            } catch (NumberFormatException ex) {
                System.err.println("java -jar Relay.jar [relay_id]");
                return false;
            }
        }
    }
}
