/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.messages.actions;

import com.tfg.relay.Constants;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePlacementEnded implements MessageAction {

    @Override
    public void action() {
        try {
            Constants.WRITER.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED);
            Constants.WRITER.flush();
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    @Override
    public int getACTION() {
        return Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED;
    }

}
