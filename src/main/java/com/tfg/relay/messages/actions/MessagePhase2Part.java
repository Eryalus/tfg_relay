/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.messages.actions;

import com.tfg.relay.Constants;
import com.tfg.relay.utils.SocketReader;
import com.tfg.relay.utils.SocketWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author eryalus
 */
public class MessagePhase2Part implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE;

    @Override
    public void action() {
        try {
            Constants.LOG.info("New phase 2 message");
            SocketReader reader = Constants.READER;
            int relayID = reader.readInt32();
            String json = reader.readString();
            Constants.LOG.info("Readed header -> " + json);
            File temp = File.createTempFile("readerPhase2_", ".tmp");
            reader.readFile(temp);
            if (relayID == Constants.RELAY_ID) {
                Constants.LOG.info("This file is for me. Relaying it...");
                SocketWriter writer = Constants.WRITER;
                writer.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PHASE_2_MESSAGE);
                writer.writeString(json);
                writer.writeFile(temp);
                writer.flush();
            }
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
