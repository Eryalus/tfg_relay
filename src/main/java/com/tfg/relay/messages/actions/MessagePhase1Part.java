/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.messages.actions;

import com.tfg.relay.Constants;
import com.tfg.relay.utils.SocketReader;
import com.tfg.relay.utils.SocketWriter;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author eryalus
 */
public class MessagePhase1Part implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE;

    @Override
    public void action() {
        try {
            Constants.LOG.info("New phase 1 message");
            SocketReader reader = Constants.READER;
            int relayID = reader.readInt32();
            long id = reader.readLong();
            int totalParts = reader.readInt32();
            int currentPart = reader.readInt32();
            long fileOffset = reader.readLong();
            String xoredFiles = reader.readString();
            String J = reader.readString();
            File temp = File.createTempFile("readerPhase1", ".tmp");
            reader.readFile(temp);
            Constants.LOG.info("Readed info -> FileID: " + id + " RelayID:" + relayID + " totalParts:" + totalParts + " currentParts:" + currentPart + " xorFiles:" + xoredFiles + " saved to " + temp.getAbsolutePath());
            if (relayID == Constants.RELAY_ID) {
                Constants.LOG.info("This file is for me. Relaying it...");
                SocketWriter writer = Constants.WRITER;
                writer.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE);
                writer.writeInt32(relayID);
                writer.writeLong(id);
                writer.writeInt32(totalParts);
                writer.writeInt32(currentPart);
                writer.writeLong(fileOffset);
                writer.writeString(xoredFiles);
                writer.writeString(J);
                writer.writeFile(temp);
                writer.flush();
            }
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
