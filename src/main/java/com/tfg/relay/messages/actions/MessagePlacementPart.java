/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.messages.actions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tfg.relay.Constants;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MessagePlacementPart implements MessageAction {

    private static final int ACTION = Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART;

    @Override
    public void action() {
        try {
            Constants.LOG.info("New placement message");
            String idsString = Constants.READER.readString();
            Constants.LOG.info("Destination users: " + idsString);
            int[] userIDs = new ObjectMapper().readValue(idsString, int[].class);
            Set<Integer> myUsers = Constants.RELAY.getConnectedUsers().keySet();
            boolean isMyUser = false;
            for (int i = 0; i < userIDs.length; i++) {
                if (myUsers.contains(userIDs[i])) {
                    isMyUser = true;
                    break;
                }
            }
            int fileID = Constants.READER.readInt32();
            int partID = Constants.READER.readInt32();
            Constants.LOG.info("New part " + fileID + "," + partID + " sended to " + idsString);
            File tempFile = File.createTempFile("subpart", ".tmp");
            Constants.LOG.info("Subfile " + fileID + "," + partID + " saved on " + tempFile.getAbsolutePath());
            Constants.READER.readFile(tempFile);
            if (isMyUser) {
                Constants.WRITER.writeInt32(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART);
                Constants.WRITER.writeString(idsString);
                Constants.WRITER.writeInt32(fileID);
                Constants.WRITER.writeInt32(partID);
                Constants.WRITER.flush();
                Constants.WRITER.writeFile(tempFile);
                Constants.LOG.info("Subfile " + fileID + "," + partID + " sended.");
            } else {
                Constants.LOG.info("Subfile " + fileID + "," + partID + " not sended.");
            }
            tempFile.delete();
        } catch (IOException ex) {
            Constants.LOG.error("", ex);
        }
    }

    @Override
    public int getACTION() {
        return ACTION;
    }

}
