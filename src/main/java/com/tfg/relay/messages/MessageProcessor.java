/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.relay.messages;

import com.tfg.relay.Constants;
import com.tfg.relay.messages.actions.MessageAction;
import com.tfg.relay.messages.actions.MessageDeliveryEnded;
import com.tfg.relay.messages.actions.MessagePhase1Part;
import com.tfg.relay.messages.actions.MessagePhase2Part;
import com.tfg.relay.messages.actions.MessagePlacementEnded;
import com.tfg.relay.messages.actions.MessagePlacementPart;
import com.tfg.relay.messages.actions.MessageSetup;
import com.tfg.relay.utils.SocketReader;
import java.util.HashMap;

/**
 *
 * @author eryalus
 */
public class MessageProcessor {

    private SocketReader reader;
    private HashMap<Integer, MessageAction> ACTIONS = new HashMap<>();

    public MessageProcessor() {
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.SETUP, new MessageSetup());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_PART, new MessagePlacementPart());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PLACEMENT_ENDED, new MessagePlacementEnded());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PHASE_1_MESSAGE, new MessagePhase1Part());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.PHASE_2_MESSAGE, new MessagePhase2Part());
        ACTIONS.put(Constants.MESSAGE_ACTIONS_TYPE.DELIVEY_ENDED, new MessageDeliveryEnded());
    }

    public void readMessageForType(int type) {
        Constants.LOG.info("Readed bytes:" + Constants.MIS.getBytesReaded());
        ACTIONS.get(type).action();
    }
}
