package com.tfg.relay.utils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.tfg.relay.Constants;
import java.io.IOException;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class MulticastOutputStream extends OutputStream {

    private static final int DATAGRAM_PACKET_SIZE = 65000 - 50;
    private final LinkedList<Byte> queue;
    private final Semaphore sem;
    private final DatagramSocket socket;
    private final InetAddress group;
    private long bytesAdded = 0, bytesSended = 0;
    private long secuenceNumber = 0;
    private final String IP;
    private ACKReceiver ackReceiver;
    private final HashMap<Integer, ArrayList<Long>> acks = new HashMap<>();
    private final Semaphore ACKS_SEM = new Semaphore(1);

    public void emptyAcks() throws InterruptedException {
        ACKS_SEM.acquire();
        for (Integer id : Constants.RELAY.getConnectedUsers().keySet()) {
            acks.put(id, new ArrayList<>());
        }
        ACKS_SEM.release();
    }

    public long getBytesAdded() {
        return bytesAdded;
    }

    public long getBytesSended() {
        return bytesSended;
    }

    public MulticastOutputStream(DatagramSocket socket, String ip) throws UnknownHostException, IOException {
        super();
        this.group = InetAddress.getByName(ip);
        this.queue = new LinkedList<>();
        sem = new Semaphore(1);
        this.socket = socket;
        this.IP = ip;
        try {
            emptyAcks();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        ackReceiver = new ACKReceiver();
        new Thread(ackReceiver).start();
    }

    private void push(byte b) throws IOException {
        bytesAdded++;
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        queue.addFirst(b);
        sem.release();
        if (queue.size() >= DATAGRAM_PACKET_SIZE) {
            sendData();
        }
    }

    private byte pop() {
        try {
            sem.acquire();
        } catch (InterruptedException ex) {
            Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        Byte removeLast = queue.removeLast();
        sem.release();
        return removeLast;
    }

    @Override
    public void flush() throws IOException {
        sendData();
    }

    private void sendData() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(secuenceNumber++);
        buffer.array();
        byte[] buf = new byte[queue.size() + buffer.array().length];
        for (int i = 0; i < buffer.array().length; i++) {
            buf[i] = buffer.array()[i];
        }
        for (int i = buffer.array().length; i < buf.length; i++) {
            buf[i] = pop();
        }
        bytesSended += buf.length;
        DatagramPacket p = new DatagramPacket(buf, buf.length, group, Constants.SERVER_MULTICAST_PORT);
        boolean done = false;
        do {
            try {
                emptyAcks();
            } catch (InterruptedException ex) {
                Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
            }
            socket.send(p);
            //wait for acks
            int counter = 0;
            while (counter < 25) {
                done = true;
                try {
                    ACKS_SEM.acquire();
                } catch (InterruptedException ex) {
                    Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (int index : acks.keySet()) {
                    if (!acks.get(index).contains(secuenceNumber - 1)) {
                        done = false;
                        break;
                    }
                }
                ACKS_SEM.release();
                if (done) {
                    break;
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
                }
                counter++;
            }
        } while (!done);
    }

    @Override
    public void write(int b) throws IOException {
        push((byte) b);
    }

    @Override
    public void write(byte[] bs) throws IOException {
        for (Byte b : bs) {
            push(b);
        }
    }

    private class ACKReceiver implements Runnable {

        MulticastSocket socket;

        public ACKReceiver() throws IOException {
            socket = new MulticastSocket(Constants.RELAY.getPort());
            InetAddress group = InetAddress.getByName(Constants.RELAY.getIp());
            socket.joinGroup(group);
        }

        @Override
        public void run() {
            byte[] buf = new byte[DATAGRAM_PACKET_SIZE];
            DatagramPacket p = new DatagramPacket(buf, DATAGRAM_PACKET_SIZE);
            while (true) {
                try {
                    socket.receive(p);
                    if (p.getLength() == 24) {
                        byte[] mBlockArray = new byte[8];
                        ByteBuffer mBlockBuffer = ByteBuffer.allocate(Long.BYTES);
                        System.arraycopy(p.getData(), 0, mBlockArray, 0, 8);
                        mBlockBuffer.put(mBlockArray);
                        mBlockBuffer.flip();
                        if (mBlockBuffer.getLong() != -1) {
                            continue;
                        }
                        byte[] idArray = new byte[8];
                        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
                        System.arraycopy(p.getData(), 8, idArray, 0, 8);
                        buffer.put(idArray);
                        buffer.flip();
                        long ID = buffer.getLong();
                        byte[] secArray = new byte[8];
                        ByteBuffer buffer2 = ByteBuffer.allocate(Long.BYTES);
                        System.arraycopy(p.getData(), 16, secArray, 0, 8);
                        buffer2.put(secArray);
                        buffer2.flip();
                        long secNumber = buffer2.getLong();
                        ACKS_SEM.acquire();
                        System.out.println("ack from " + ID + " to " + secNumber);
                        acks.get((int) ID).add(secNumber);
                        ACKS_SEM.release();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MulticastOutputStream.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
